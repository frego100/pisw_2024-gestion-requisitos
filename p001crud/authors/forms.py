from django import forms
from .models import Author

class AuthorForm(forms.ModelForm):
    AutCod=forms.CharField(
        label="Código"
    )
    AutVer=forms.FloatField(
        label="Versión"
    )
    AutFecIng=forms.DateField(
        label="Fecha de Ingreso"
    )
    AutNom=forms.CharField(
        label="Nombre"
    )
    AutApePat=forms.CharField(
        label="Apellido Paterno"
    )
    AutApeMat=forms.CharField(
        label="Apellido Materno"
    )
    AutTel=forms.IntegerField(
        label="Teléfono"
    )
    AutDNI=forms.IntegerField(
        label="DNI"
    )    
    AutOrgCod_id=forms.CharField(
        label="Codigo de Organizacion"
    )
    AutEstCod_id=forms.CharField(
        label="Estado"
    )

    AutCom=forms.CharField(
            widget=forms.Textarea(attrs={'rows': 4,'class': 'comentario'}),
            label= 'Comentarios'
        )
    class Meta:
        model = Author
        fields = '__all__'
        labels = {
                'AutCod': 'Código del Autor',
                'AutVer': 'Versión',
                'AutFecIng': 'Fecha de Ingreso',
                'AutAli': 'Alias del Autor',
                'AutCom': 'Comentario',
                'AutApeMat': 'Apellido materno',
                'AutApePat': 'Apellido paterno',
                'AutNom': 'Nombre de Autor',
                'AutCon': 'Contraseña de Autor',
                'AutTel': 'Telefono de Autor',
                'AutDNI': 'DNI de Autor',
                'AutOrgCod_id': 'Codigo de Organizacion',
                'AutEstCod_id': 'Estado',
        }
