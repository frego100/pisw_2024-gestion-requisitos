from django import forms
from .models import Organization
from .models import OrganizationType
class OrganizationForm(forms.ModelForm):
    OrgFecCre = forms.DateField(
        widget=forms.DateInput(format='%d/%m/%Y'),
        input_formats=['%d/%m/%Y'],
        label= 'Fecha de Creación'
    )
    # OrgFecMod = forms.DateField(
    #     widget=forms.DateInput(format='%d/%m/%Y'),
    #     input_formats=['%d/%m/%Y']
    # )
    OrgCod = forms.CharField(
        label = 'Código'
        # widget=forms.TextInput(attrs={'disabled': 'disabled'})
    )
    OrgVer = forms.CharField(
        label= 'Versión'
        #  widget=forms.TextInput(attrs={'disabled': 'disabled'})
    )
    OrgNom = forms.CharField(
        label= 'Nombre'
    )
    OrgDir = forms.CharField(
        label= 'Dirección'
    )
    OrgTel = forms.IntegerField(
        label= 'Teléfono'
    )
    OrgRepLeg = forms.CharField(
        label= 'Representante Legal'
    )
    OrgTelRepLeg = forms.IntegerField(
        label= 'Teléfono Representante Legal'
    )
    OrgRuc = forms.IntegerField(
        label= 'Ruc'
    )
    OrgContact = forms.CharField(
        label= 'Contacto'
    )
    OrgTelCon = forms.IntegerField(
        label= 'Teléfono del Contacto'
    )
    OrgAutCod = forms.CharField(
        label= 'Autor de Plantilla'
    )
    OrgCom=forms.CharField(
        widget=forms.Textarea(attrs={'rows': 4,'class': 'comentario'}),
        label= 'Comentarios'
    )
    OrgTipOrgCod_id = forms.ModelChoiceField(
        queryset=OrganizationType.objects.all(),
        widget=forms.Select,
        label="Tipo de Organización",
        initial="Contratado"
    )
    class Meta:
        model = Organization
        fields = '__all__'  # Todos los campos del modelo
        labels = {
            'OrgCod': 'Código de la Organización',
            'OrgVer': 'Versión de la Organización',
            'OrgNom': 'Nombre del Organización',
            'OrgFecCre': 'Fecha de Creación',
            'OrgFecMod': 'Fecha de Modificación',
            'OrgCom': 'Comentario',
            'OrgCodModPlan': 'Codigo modificación Plantilla',
            'OrgTipOrgCod': 'Código Tipo de Organización',
            'OrgAutCod': 'Autor',
            'OrgDir': 'Direccion',
            'OrgTel': 'Telefono',
            'OrgRepLeg': 'Representante Legal',
            'OrgTelRepLeg': 'Telefono Representante Legal',
            'OrgRuc': 'RUC de la Organización',
            'OrgContact': 'Contacto la Organización',
            'OrgTelCon': 'Telefono Contacto la Organización',
            'OrgArtCod': 'Artefacto',
        }

    def __init__(self, *args, **kwargs):
        # Check if 'disabled_fec_cre' is in kwargs and use it to set the 'disabled' attribute
        disabled = kwargs.pop('disabled', False)
        super().__init__(*args, **kwargs)
        if disabled:
            self.fields['OrgFecCre'].widget.attrs['disabled'] = 'disabled'
            self.fields['OrgCod'].widget.attrs['disabled'] = 'disabled'
            self.fields['OrgVer'].widget.attrs['disabled'] = 'disabled'

    def clean_OrgCod(self):
        pro_cod = self.cleaned_data.get('OrgCod')
        # Validación personalizada para OrgCod
        if not pro_cod.isalnum():
            raise forms.ValidationError('El código del proyecto debe ser alfanumérico.')
        return pro_cod

    def clean_OrgVer(self):
        pro_ver = self.cleaned_data.get('OrgVer')
        # Validación personalizada para OrgVer
        if not pro_ver:
            raise forms.ValidationError('La versión del proyecto es obligatorio.')
        return pro_ver

    def clean_OrgNom(self):
        pro_nom = self.cleaned_data.get('OrgNom')
        # Validación personalizada para OrgNom
        if not pro_nom:
            raise forms.ValidationError('El nombre del proyecto es obligatorio.')
        return pro_nom

    def clean_OrgEst(self):
        pro_est = self.cleaned_data.get('OrgEst')
        # Validación personalizada para OrgEst
        valid_states = ['En progreso','Concluido']
        if pro_est not in valid_states:
            raise forms.ValidationError(
                'El estado del proyecto debe ser uno de los siguientes: En progreso, Concluido.'
            )
        
        return pro_est
    
    def clean_OrgTipOrgCod_id(self):
        org_tip = self.cleaned_data.get('OrgTipOrgCod_id')
        if org_tip not in ['TIP-ORG-001', 'TIP-ORG-002']:
            raise forms.ValidationError('El tipo de organización debe ser uno de los siguientes: Contratada, Contratante.')
        return org_tip
    
  
    def clean(self):
        cleaned_data = super().clean()
        pro_fec_cre = cleaned_data.get('OrgFecCre')
        pro_fec_mod = cleaned_data.get('OrgFecMod')

        # Validación general
        if pro_fec_cre and pro_fec_mod and pro_fec_cre > pro_fec_mod:
            self.add_error('OrgFecCre', 'La fecha de creación no puede ser posterior a la fecha de modificación.')
            self.add_error('OrgFecMod', 'La fecha de modificación no puede ser anterior a la fecha de creación.')

        return cleaned_data
