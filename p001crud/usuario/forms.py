from django import forms
from .models import Role, Educcion, Entrevista, Evidence, CategoryEvidence, Ilacion, Especificacion

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

class RoleForm(forms.ModelForm):
    class Meta:
        model = Role
        fields = ['RolCod', 'RolNom', 'RolCom', 'RolOrgCod']
        labels = {
            'RolCod': 'Código del Rol',
            'RolNom': 'Nombre del Rol',
            'RolCom': 'Comentarios',
            'RolOrgCod': 'Código de Organización',
        }
        widgets = {
            'RolOrgCod': forms.Select(),
        }

class EduccionForm(forms.ModelForm):
    ilaciones = forms.ModelMultipleChoiceField(
        queryset=Ilacion.objects.all(),
        required=False,
        widget=forms.CheckboxSelectMultiple,
        label='Ilaciones'
    )

    class Meta:
        model = Educcion
        fields = '__all__'
        labels = {
            'EduCod': 'Código de Educción',
            'EduNom': 'Nombre del Educción',
            'EduVer': 'Versión del Educción',
            'EduCom': 'Comentarios',
            'EduFecMod': 'Fecha de modificación',
            'EduEstCod': 'Estado',
            'EduCom': 'Comentario',
            'EduFueCod': 'Fuente',
            'EduExpCod': 'Experto',
            'EduActCod': 'Actor',
            'EduArtCod': 'Artefacto',
        }
        exclude = ['EduProCod','EduAutCod']

    def __init__(self, *args, **kwargs):
        super(EduccionForm, self).__init__(*args, **kwargs)
        if self.instance.pk:
            self.fields['ilaciones'].initial = self.instance.ilaciones.all()

    def save(self, commit=True):
        instance = super(EduccionForm, self).save(commit=False)
        if commit:
            instance.save()
            self.save_m2m()
        return instance

    def save_m2m(self):
        self.instance.ilaciones.set(self.cleaned_data['ilaciones'])

class IlacionForm(forms.ModelForm):
    class Meta:
        model = Ilacion
        fields = '__all__'
        labels = {
            'IlaCod': 'Código de Ilación',
            'IlaNom': 'Nombre del Ilación',
            'IlaVer': 'Versión del Ilación',
            'IlaCom': 'Comentarios',
            'IlaFecMod': 'Fecha de modificación',
            'IlaEstCod': 'Estado',
            'IlaEduCod': 'Código de Educción',
            'IlaCom': 'Comentario',
            'IlaFueCod': 'Fuente',
            'IlaExpCod': 'Experto',
            'IlaActCod': 'Actor',
            'IlaArtCod': 'Artefacto',
        }
        exclude = ['IlaProCod','IlaAutPlanCod']

class EspecificacionForm(forms.ModelForm):
    class Meta:
        model = Especificacion
        fields = '__all__'
        labels = {
            'EspCod': 'Código de Especificacion',
            'EspNom': 'Nombre del Especificacion',
            'EspVer': 'Versión del Especificacion',
            'EspCom': 'Comentarios',
            'EspFecMod': 'Fecha de modificación',
            'EspEstCod': 'Estado',
            'EspIlaCod': 'Código de Ilación',
            'EspCom': 'Comentario',
            'EspFueCod': 'Fuente',
            'EspExpCod': 'Experto',
            'EspActCod': 'Actor',
            'EspArtCod': 'Artefacto',
        }
        exclude = ['EspProCod','EspAutPlanCod']

class EntrevistaForm(forms.ModelForm):
    EntrCom=forms.CharField(
        widget=forms.Textarea(attrs={'rows': 4,'class': 'comentario'}),
        label= 'Comentarios'
    )
    class Meta:
        model = Entrevista
        fields = '__all__'
        labels = {
            'EntrCod': 'Código de Entrevista',
            'EntrVer': 'Versión del Entrevista',
            'EntrCom': 'Comentarios',
            'EntrFecMod': 'Fecha de modificación',
            'EntrEstCod': 'Estado',
            'EntrNomPer': 'Nombre Entrevistado',
            'EntrCargPer': 'Cargo Entrevistado',
            'EntrCom': 'Comentario',
            'EntrFueCod': 'Fuente',
            'EntrExpCod': 'Experto',
            'EntrActCod': 'Actor',
            'EntrArtCod': 'Artefacto',
        }
        exclude = ['EntrProCod','EntrAutCod']

class CategoryEvidenceForm(forms.ModelForm):
    class Meta:
        model = CategoryEvidence
        fields = ['CatEviCod', 'CatEviNom']
        labels = {
            'CatEviCod': 'Codigo de Categoria Evidencia',
            'CatEviNom': 'Nombre de Categoria Evidencia'
        }

class EvidenceForm(forms.ModelForm):
    class Meta:
        model = Evidence
        fields = ['EviCod', 'EviNom', 'EviEntCod', 'EviFec', 'EviArc', 'EviCatEviCod', 'EviArtCod']
        labels = {
            'EviCod': 'Código de la Evidencia',
            'EviNom': 'Nombre de la Evidencia',
            'EviEntCod': 'Código de la Entrevista',
            'EviFec': 'Fecha de la Evidencia',
            'EviArc': 'Archivo de la Evidencia',
            'EviCatEviCod': 'Categoría de la Evidencia',
            'EviArtCod': 'Código del Artefacto'
        }
        
        widgets = {
            'EviFec': forms.DateInput(attrs={'type': 'date'}),
        }
